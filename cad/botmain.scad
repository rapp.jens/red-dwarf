/* 
 * tiny cambot
 * utilizes esp32cam and raspberry pi pico
 */
use <electrics/esp32.scad>
use <electrics/arduino.scad>
use <electrics/schalter.scad>
use <motors/sg90.scad>
use <omniwheel.scad>

function milraster(x) = 2.54 * x;

$fn=($preview) ? 0 : 100;

/***
 * electrics: tbd: make it nice
 */
module rppico() {
	color("green") cube([21,51,1],center=true);
}

/***
 * the omni wheel with parameters
 */
module wheel_center() {
	omniwheel_base();
}
module wheel() {
	omniwheel();
}
module wheel_front() {
	omniwheel_front(dia = 40) {translate([0,0,.5])rotate([0,0,45]) sg90_doublehorn_mask();}	
}
module wheel_roll() {
	omniwheel_roll(d1=8);
}


module pcb_holemask(d=3,l=5,w1=58.42,w2=30.48,h=25.390) {
	w1h = w1/2;
	w2h = w2/2;
	hh = h/2;
	
	translate([-w1h, hh])cylinder(d=d,h=l,center=true);
	translate([ w1h, hh])cylinder(d=d,h=l,center=true);
	translate([ w2h,-hh])cylinder(d=d,h=l,center=true);
	translate([-w2h,-hh])cylinder(d=d,h=l,center=true);
}

module pcb() {
	w1 = 17.78;
	w2 = 33.02;
	h1 = 66.04;
	h2 = 36.83;
	
	difference() {
		color("green")hull() {
			translate([-(w2-w1)/2,0]) cube([w1,h1,1],center=true);
			translate([0,0]) cube([w2,h2,1],center=true);
			
		}
		rotate([0,0,90])pcb_holemask();
	}	
	
	translate([-6,0,4])arduino_nano();
}

/**
 * helpsters
 */
module rounded_rect(dim=[10,10,2],r=3,center=false) {
	x2=dim[0]/2-r;
	y2=dim[1]/2-r;
	h=dim[2];
	hull() {
		translate([-x2,-y2])cylinder(r=3,h=h,center=center);
		translate([-x2, y2])cylinder(r=3,h=h,center=center);
		translate([ x2,-y2])cylinder(r=3,h=h,center=center);
		translate([ x2, y2])cylinder(r=3,h=h,center=center);
	}
	
}

module neck_stick() {
	$fn=20;
	difference() {
		union() {
			translate([0,0,-2])cylinder(d=9,h=6,center=true);

			hull() {
				cylinder(d=9,h=2,center=true);
				translate([0,-50,0])cylinder(d=9,h=2,center=true);
			}
		}
		cylinder(d=3,h=13,center=true);
		translate([0,-50,0])cylinder(d=3,h=3,center=true);
	}	
}

module neck_stick_b() {
	$fn=25;
	difference() {
		union() {
			translate([0,-50,-2]) {
				cylinder(d=9,h=6,center=true);
				hull() {
					translate([0,50])cylinder(d=9,h=6,center=true);
					translate([0,30,0])cylinder(d=9,h=6,center=true);
				}
			}
			hull() {
				cylinder(d=9,h=3,center=true);
				translate([0,-50,0])cylinder(d=9,h=3,center=true);
			}
		}
		rotate([0,0,180]){
			sg90_singlehorn_holemask();
			translate([0,0,1])sg90_singlehorn_mask();
		}
		
		cylinder(d=5,h=13,center=true);
		translate([0,-50,0])cylinder(d=3,h=10,center=true);
	}		
}

/**
 * these are Body parts
 */

module base_mount_mask(d=3,l=10) {
	translate([-12,-30,0])cylinder(d=d,h=l,center=true);
	translate([ 12,-30,0])cylinder(d=d,h=l,center=true);
	translate([ 12, 30,0])cylinder(d=d,h=l,center=true);
	translate([-12, 30,0])cylinder(d=d,h=l,center=true);	
}


module neck_mount() {
	difference() {
		translate([0,0,-4])rounded_rect([70,32,12]);
		rotate([0,0,90])base_mount_mask(d=2,l=20);
		translate([0,-16,0])cube([70,15,8],center=true);
		translate([0, 16,0])cube([70,15,8],center=true);
		translate([-38,0,4])cube([26,8,9],center=true);
		translate([-28,0,4])cube([6,12,9],center=true);
		translate([ 38,0,4])cube([26,8,9],center=true);
		translate([ 28,0,4])cube([6,12,9],center=true);
		translate([  0,0,-2])rotate([0,90,0])cylinder(d=25,h=38,center=true);
		translate([  0,0,-4])cube([90,12.5,12.5],center=true);
		translate([0,0,4.75])rotate([0,90,0])cylinder(d=2,h=100,center=true, $fn=12);
	}
	
}

module body_base() {
	difference() {
		// base shape
		translate([0,0,1])cylinder(d=135,h=14,center=true);
		// inner structure
		difference() {
			cylinder(d=130,h=12,center=true);
			divide(3,70,30){
				cube([30,70,20],center=true);
			}
		}

		// motor mountss
		divide(3,70,30){
			cube([14,70,12],center=true);
			rotate([90,0,90])sg90_holemask(r=0.8, l=60,corpus=true);
		}
		
		// mount mask for mechanics
		divide(3,-42,30) {
			base_mount_mask(l=30);
		}
		
		// mount holes
		divide(9,67,30) {
			rotate([0,90,0])cylinder(d=3,h=10,center=true);
		}
		cylinder(d=50,h=30,center=true);
	}
	
}


/**
 * head
 */

module head_mount_hole_mask(d=2,l=5) {
	$fn=16;
	translate([0,-37])cylinder(d=d,h=l, center=true);
	translate([-17,10])cylinder(d=d,h=l, center=true);
	translate([ 17,10])cylinder(d=d,h=l, center=true);
}

module head_bottom() {
	difference() {
		union() {
			// base shape
			hull(){
				cube([40,30,3],center=true);
				translate([0,-20])cylinder(d=40,h=3,center=true);
			}
			// neck stick mount
			translate([0,-20,6])cylinder(d=28,h=10,center=true);
			translate([0,-20,13])cylinder(d1=28, d2=24,h=4,center=true);
		}
		// left and right
		translate([-21,-24,9.5])cube([33,40,16],center=true);
		translate([21,-24,9.5])cube([33,40,16],center=true);
		
		// mount holes for neck sticks
		translate([0,-20,8])rotate([0,90,0]) {
			cylinder(d=3,h=20,center=true,$fn=12);
			translate([0,-8.5])cylinder(d=3,h=20,center=true,$fn=12);
			translate([0, 8.5])cylinder(d=3,h=20,center=true,$fn=12);
		}

		// screw lashes
		translate([0,-20,8])rotate([0,90,0]) {
			translate([0, 0, 4])cylinder(d=7,h=5,center=true,$fn=20);
			translate([0,-8.5,-4])cylinder(d=7,h=5,center=true,$fn=20);
			translate([0, 8.5,-4])cylinder(d=7,h=5,center=true,$fn=20);
		}

		// mount holes
		head_mount_hole_mask(d=3);
	}
	
}

module head_top_base(d=0) {
	d2 = d+d;
	hull() {
		//bottom
		translate([0,0,-d])cube([40-d2,30-d2,1],center=true);
		translate([0,-20,4-d])cylinder(d=40-d2,h=9,center=true);
		//top
		translate([0,-20,12-d])cylinder(d=25-d2,h=15,center=true);
		translate([0,0,12-d])cylinder(d=19-d2,h=15,center=true);

		// nose(cam)
		translate([0,10,10])rotate([90,90,0])cylinder(d=19-d2,h=10-d2,center=true);
		// ear
		translate([0,-10,8])rotate([0,90])cylinder(d=12-d2,h=40-d2,center=true);
		
	}
}

module head_top() {
	difference() {
		head_top_base();
		difference() {
			head_top_base(2);
			// mount holes
			head_mount_hole_mask(d=9,l=30);
		}
		// cam hole.. tape the cam in
		translate([0,10,8])rotate([90,0])cylinder(d=9,h=12,center=true, $fn=26);
		// mount holes
		head_mount_hole_mask(l=10);
		// cable 
		translate([0,-35,14])rotate([70,0])cylinder(d=8,h=10,center=true,$fn=20);
		// ear
		translate([0,-10,8])rotate([0,90])cylinder(d=2,h=42,center=true,$fn=12);
		
	}
	
	translate([0,-13,1])rotate([0,180,0])%esp32_cam(with_cam=false);
}

module head() {
	rotate([0,180,0])head_bottom();
	translate([0,0,2])head_top();
}

	
/**
 * here comes the case
 */
module case_base_shape(d=0) {
	d2 = d+d;
	translate([0,0,2]){
		cylinder(d=141-d2,h=17,center=true);
		translate([0,0,8.5])intersection() {
			sphere(d=141-d2);
			translate([0,0,16-d])cube([200,200,36],center=true);
		}
	}
	
}

module gripper_base_shape(d=0,h=200) {
	hull() {
		translate([0, 0,20])rotate([0,90,0])cylinder(d=16-d,h=h,center=true);
		translate([0,-45,24])rotate([0,90,0])cylinder(d=25-d,h=h,center=true);
	}	
}

module case() {
	difference() {
		case_base_shape();
		difference() {
			case_base_shape(2);
		}
		// motor mounts
		divide(3,70,30){
			cube([14,70,14],center=true);
		}

		// mount holes
		divide(9,67,30) {
			rotate([0,90,0])cylinder(d=3,h=10,center=true);
		}
		
		// head hole
		translate([0,-8,40])hull() {
			cylinder(d=42,h=20,center=true);
			translate([0,30])cube([42,30,20],center=true);
			//translate([0,60,0])scale([1,0.4,1])cylinder(d=55,h=20,center=true);
		}
		translate([0,38,40])hull(){
			translate([0,25,5])scale([1,1,.4])rotate([90,0])cylinder(d=55,h=20,center=true);
			scale([1,.4,1])cylinder(d=55,h=20,center=true);
		}
		// leave space for the cable
		translate([0,-30,40])cylinder(d=12,h=20,center=true);
		
		// finally, where the grippers may be..
		gripper_base_shape();
	}
}

module gripper_case_shape_r() {
	intersection() {
		case_base_shape();
		translate([50,0])gripper_base_shape(1,100);
	}	
}


module multimount_base_shape() {
	difference() {
		union() {
			cylinder(d=150,h=3,center=true);

			translate([0,0,-9])divide(3,-42,30) {
				translate([-12, 30,0])cylinder(d=9,h=18,center=true);
				translate([-12, -30,0])cylinder(d=9,h=18,center=true);
			}
		}
		// remove front
		translate([0,60,0])cube([140,60,50],center=true);
		// remove neck part
		translate([0,30,0])cube([30,80,5],center=true);
		translate([0,-10])cylinder(d=30,h=10,center=true);

	}
	
	translate([-59,28])cylinder(d=20,h=3,center=true);
	translate([ 59,28])cylinder(d=20,h=3,center=true);
}
/**
 * 2nd floor, panties, gripper mounts and microcontrollers
 */
module multi_mount() {
	arm_pos = 15;
	arm_disp = 57;
	difference() {
		multimount_base_shape();
		// servo positioning
		translate([63,10,8])rotate([0,0,180])sg90_holemask(r=1,corpus=true, l=20);
		translate([-63,10,8])rotate([0,0,0])sg90_holemask(r=1,corpus=true, l=20);

		translate([-63,-10])cylinder(d=5,h=10,center=true,$fn=20);
		translate([63,-10])cylinder(d=5,h=10,center=true,$fn=20);
		
		// mount mask for mechanics
		divide(3,-42,30) {
			translate([-12, 30,0])cylinder(d=3,h=50,center=true,$fn=16);
			translate([-12, -30,0])cylinder(d=3,h=50,center=true,$fn=16);
		}

		// pcb
		translate([38,-30,2])rotate([0,0,90])#pcb_holemask(d=1.5,l=20);
		translate([-38,-30,2])rotate([0,0,-90])#pcb_holemask(d=1.5,l=10);
	
		// montage für Arme
		translate([-60,30])cylinder(d=5,h=4,center=true,$fn=20);
		translate([ 60,30])cylinder(d=5,h=4,center=true,$fn=20);
		
		// Schalter
		translate([0,-70])rotate([0,0,90])#schiebeschalter6x12_holemask();
		translate([10,-60])cylinder(d=10,h=10,center=true,$fn=16);
		
		// kabeldurchführungen
		translate([0,-30]){
			cylinder(d=12,h=10,center=true,$fn=16);
			translate([0,6])cube([12,12,10,],center=true);
		}
		translate([-30,10])cylinder(d=12,h=10,center=true,$fn=16);
		translate([ 30,10])cylinder(d=12,h=10,center=true,$fn=16);
		
	}
	
	translate([63,10,8.5])rotate([0,0,180]){
		%sg90();
		rotate([0,0,0])%sg90_singlehorn();
	}
	translate([-63,10,8.5])rotate([0,0,0]){
		%sg90();
		rotate([0,0,90])%sg90_singlehorn();
	}
	
	translate([38,-30,2])rotate([0,0,0])%pcb();
}

/**
 * mount the parts together
 */

module body(rh=0, tilt=0) {
	color("lightblue")body_base();
	// first, the wheels
	
	divide(3,70,30) {
		rotate([90,0,90]){
			%sg90();
			%sg90_doublehorn();
			translate([0,0,16])wheel();
		}
	}
	
	// next, the neck
	translate([0,42,16]){
		rotate([0,180,0])neck_mount();
		translate([-20,0,15])rotate([0,90,0]){
			%sg90();
			rotate([0,0,-tilt]) {
				%sg90_doublehorn();
				translate([0,8.50,15])neck_stick();
				translate([0,-58.50,11])rotate([0,180,180])neck_stick();
				translate([-8,-30,20])rotate([0,-90,0])head();
			}
		}
		translate([ 20,0,15])rotate([0,90,180]){
			%sg90();
			rotate([0,0,tilt]) {
			%sg90_singlehorn();
			translate([0,0,9])rotate([180,0])neck_stick_b();
			}
		}
	}

	// 2nd floor for electronics, mount for the grippers
	translate([0,0,26])multi_mount();
	//case();

}

//body(tilt=00);
//body(rh=0,tilt=9);
//wheel_front();
//wheel_center();
	
//head_top();
//head_bottom();
//rotate([180,0])neck_stick();
//neck_stick_b();
//rotate([180,0])neck_mount();
//rotate([180,0])multi_mount();
projection()multi_mount();