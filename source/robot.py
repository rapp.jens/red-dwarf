from machine import Pin, PWM, Timer

def __init_servo(pin):
    # print('initializing servo on pin {}'.format(pin))
    servo = PWM(Pin(pin))
    servo.freq(50)
    servo.duty_ns(1500000)
    return servo

class Servo:
    def __init__(self, pin, accel=2):
        self.servo = PWM(Pin(pin))
        self.servo.freq(50)
        self.position = 0
        self.destination = 0
        self.acceleration = accel

    def run(self):
        disp = self.position - self.destination
        if self.acceleration >= abs(disp):
            self.position = self.destination
        elif disp < 0:
            self.position = self.position + self.acceleration
        elif disp > 0:
            self.position = self.position - self.acceleration

    def set_destination(self, value):
        self.destination = value

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, value):
        '''
        set position in range -100 to +100
        '''
        self._position = value
        base = 1.5 + (value/100) * 0.5
        self.servo.duty_ns(int(base * 1000000))


class Robot:
    servostep = 2
    LEFT = 0
    RIGHT = 1
    BACK = 2
    TILT = 3
    NECK = 4

    def __init__(self):
        motors = [ (21, 2), (20, 2), (19, 2), (18,5), (17, 5) ]
        self.servos = [ Servo(pin, acc) for pin, acc in motors ]
        self.timer = Timer(period=50, mode=Timer.PERIODIC, callback=self.animate)


    def animate(self, t):
        for servo in self.servos:
            servo.run()

    def set_servo(self, number, val):
        self.servos[number].set_destination(val)

    def stop(self):
        self.turn(0)

    def turn(self,speed=30):
        '''
        turn around
        '''
        self.set_servo(Robot.LEFT, speed)
        self.set_servo(Robot.RIGHT, speed)
        self.set_servo(Robot.BACK, speed)

    def roll(self, speed=30):
        '''
        forward/backward
        '''
        self.set_servo(Robot.LEFT, speed)
        self.set_servo(Robot.RIGHT, -speed)

    def nick(self, position=0):
        '''
        set neck position
        '''
        self.set_servo(Robot.NECK, position)

    def tilt(self, position=0):
        '''
        look up and down
        '''
        self.set_servo(Robot.TILT, position)


def testrun(bot):
    import time
    bot.roll(30)
    time.sleep(1)
    bot.stop(0)
    time.sleep(1)
    bot.run(-30)
    time.sleep(1)
    bot.stop(0)
    bot.turn(50)
    time.sleep(1)
    bot.turn(-50)
    time.sleep(1)
    bot.stop(0)
    bot.tilt(-70)
    time.sleep(1)
    bot.nick(90)
    bot.tilt(90)

