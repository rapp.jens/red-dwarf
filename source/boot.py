import network
import json
import os

def connect_wifi(config):
    if not 'ssid' in config:
        return
    if not 'key' in config:
        return
    SSID = config['ssid']
    KEY = config['key']

    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.connect(SSID, KEY)
    # We should have a valid IP now via DHCP
    print("Wi-Fi Connected ", wlan.ifconfig())

def init_system():
    filename = 'sys.json'
    if filename in os.listdir('/'):
        with open(filename) as fp:
            data = json.load(fp)
            if 'wifi' in data:
                connect_wifi(data['wifi'])

# initialize
init_system()
