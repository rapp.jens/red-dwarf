from robot import Robot
from remote_control import Server

class RobotControl(Server):
    def __init__(self, port=8090):
        super().__init__(port)
        self.robot = Robot()
        self.set_handler('roll', self.roll)
        self.set_handler('turn', self.turn)
        self.set_handler('stop', self.stop)
        self.set_handler('nick', self.nick)
        self.set_handler('tilt', self.tilt)

    def roll(self, order, args):
        speed = 30
        if len(args) > 1:
            speed = int(args[1])

        self.robot.roll(speed)

    def turn(self, order, args):
        speed = 30
        if len(args) > 1:
            speed = int(args[1])

        self.robot.turn(speed)

    def nick(self, order, args):
        speed = 30
        if len(args) > 1:
            speed = int(args[1])

        self.robot.nick(speed)

    def tilt(self, order, args):
        speed = 30
        if len(args) > 1:
            speed = int(args[1])

        self.robot.tilt(speed)

    def stop(self, order, args):
        self.robot.stop()

