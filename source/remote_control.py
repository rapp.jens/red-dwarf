from machine import WDT
import socket
import _thread

class Server:
    '''
    server system for remote control
    '''
    def __init__(self, port=8090, message="Server v0.1 ok."):
        '''
        Initialize the thread
        '''
        self.port = port
        self.message = message
        self.lock = _thread.allocate_lock()
        self.orders = {}
        self.set_handler('__ping__', self.handle_ping)


    def handle_order(self, client, line):
        '''
        handle a request
        '''
        parameters = line.split(' ')
        order = parameters[0]
        print ('having order "{}"'.format(order))
        if order in self.orders:
            print(' - order found.')
            self.orders[order](client, parameters)
            self.send(client, 'ok.')
        else:
            self.send(client, '?.')

    def set_handler(self, order, function):
        '''
        set a handler for an order
        '''
        self.orders[order] = function

    def handle_ping(self, client, args):
        '''
        test handler for pings
        '''
        self.send(client, '__pong__')

    def run(self):
        _thread.start_new_thread(self.dorun, ())

    def dorun(self):
        '''
        create the server
        '''
        s = socket.socket()
        s.settimeout(1)
        s.bind(('127.0.0.1', self.port ))
        s.listen(0)

        while True:
            print("waiting for client")

            try:
                client, addr = s.accept()
            except OSError:
                pass
            except:
                raise
            else:
                print("client accepted")

                while True:
                    try:
                        content = client.readline().strip().decode('utf-8')
                    except OSError:
                        pass
                    except:
                        raise
                    else:
                        print('recv: {}'.format(content))
                        if len(content) == 0:
                           break
                        else:
                            self.lock.acquire()
                            self.handle_order(client, content)
                            self.lock.release()
                print("Closing connection")
                client.close()

    def send(self, socket, data):
        '''
        send data to a client
        '''
        s = data
        if s[-1] != '\n':
            s = s + '\n'
        socket.write(s)

class Client:
    '''
    this will be the client socket.
    doesn't do much but looks like an api
    '''
    def __init__(self, host, port=8090):
        '''
        connect to the server
        '''
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        addr = socket.getaddrinfo(host, port)[0][4]
        self.socket.connect(addr)

    def send (self, data):
        '''
        send data to the server
        '''
        s = data
        if s[-1] != '\n':
            s = s + '\n'
        self.socket.write(s)

    def read(self):
        '''
        read from socket
        '''
        return self.socket.readline()
